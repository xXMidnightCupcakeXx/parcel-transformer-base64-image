import { Transformer } from '@parcel/plugin';
import { lookup } from 'mime-types';

exports.default = (new Transformer({
  async loadConfig({config}) {
    let result = await config.getConfigFrom('~', ['.base64imagerc', '.base64imagerc.js'], {
      packageKey: 'parcel-transformer-base64-image',
    });

    const ignoreFiles = result ? result.contents.ignoreFiles :  null;
    const emitFiles = result ? result.contents.emitFiles : false;

    config.setResult({ ignoreFiles, emitFiles });
  },

  async transform({asset, config}) {  
    if(config.ignoreFiles) {
      let match = config.ignoreFiles.some((rx) => {
        return rx.test(asset.filePath);
      });
  
      if(match) {
        if(config.emitFiles){
          asset.isIsolated = true;
        } else {
          asset.isInline = true;
          asset.meta.inlineType = 'string';
          asset.setCode('');
        }
        return [asset];
      } 
    }

    asset.isInline = true;
    asset.meta.inlineType = 'string';
    const payload = await asset.getBuffer(); 
  
    const mimeType = lookup(asset.filePath);
    if (mimeType === false) throw new Error(`cannot determine the mime type for ${asset.filePath}`);
  
    const dataURI = 'data:' + mimeType + ';base64,' + payload.toString('base64');
  
    asset.setCode(dataURI);
    return [asset];
  },
}));